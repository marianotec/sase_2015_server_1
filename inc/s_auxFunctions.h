/**
 * @file   server_auxiliar_functions.h
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   15/7/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 */

#ifndef INC_S_AUXFUNCTIONS_H_
#define INC_S_AUXFUNCTIONS_H_

/* **************************************************************** */
/* 								INCLUDEs							*/

#include <unistd.h>

#include <string.h>

/* **************************************************************** */
/* 								DEFINEs								*/

// CONFIGURATION DEFINEs

// GENERIC DEFINEs

// FUNCTIONAL DEFINEs (DO NOT CHANGE)

// OTHER DEFINEs

#define 	WRITE_ERROR_CODE		-1

/* **************************************************************** */
/* 								IFDEFs								*/

//	Add ifdef's here

/* **************************************************************** */
/* 								MACROs								*/

//	Add macro's here

/* **************************************************************** */
/* 						     EXTERNAL GLOBALs						*/

//	Add global variable's here

/* **************************************************************** */
/* 						     	PROTOTYPEs							*/

void	exitFunction	(int *exit, int wFD, const char *message);

int		writeTo	(int wFD, const char * message);


#endif /* INC_S_AUXFUNCTIONS_H_ */
