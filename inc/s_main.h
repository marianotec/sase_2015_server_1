/**
 * @file   c_main.h
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   31/7/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 */

#ifndef INC_S_MAIN_H_
#define INC_S_MAIN_H_

#pragma once

/* **************************************************************** */
/* 								INCLUDEs							*/

#include	<stdio.h>

#include	<stdlib.h>

#include	<unistd.h>

#include	<strings.h>

#include	<sys/select.h>

#include	<sys/types.h>

#include	<sys/socket.h>

#include	<netinet/in.h>

#include	<arpa/inet.h>

#include	<errno.h>

#include	<netdb.h>

#include	"s_tcp_udp.h"

#include	"s_auxFunctions.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// CONFIGURATION DEFINEs

#define		COMMAND_BUFFER_SIZE		50

#define		MAX_MESSAGE				20

#define		TCP_SERVER_PORT			3490

#define		UDP_SERVER_PORT			(TCP_SERVER_PORT+1)

#define		MAX_CONNECTIONS			5

#define		USEC_SELECT_TIMEOUT		10000

#define		SEC_SELECT_TIMEOUT		0

// GENERIC DEFINEs

// FUNCTIONAL DEFINEs (DO NOT CHANGE)

#define 	KEYBOARD_FD				0

#define		TRUE					1

#define		FALSE					0

#define 	LOCALHOST				"127.0.0.1"

// OTHER DEFINEs

#define		TIMEOUT_VAL				0

/* **************************************************************** */
/* 								IFDEFs								*/

//	Add ifdef's here

/* **************************************************************** */
/* 								MACROs								*/

#define		IS_ERROR(x)					( (0 > x)  ? 1:0 )

#define		IS_TIMEOUT(x)				( (0 == x) ? 1:0 )

#define 	KEEP_RUNNING(x)				( (0 == x) ? 1:0 )

#define 	IS_CHILD_PROCCESS(x)		( (0 == x) ? 1:0 )

/* **************************************************************** */
/* 						     EXTERNAL GLOBALs						*/

//	Add global variable's here

/* **************************************************************** */
/* 						     	PROTOTYPEs							*/

#endif /* INC_S_MAIN_H_ */
