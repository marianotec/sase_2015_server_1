/**
 * DOXYGEN COMMENTS
 *
 * @file   c_tcp.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   31/7/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include "s_tcp_udp.h"

/* **************************************************************** */
/* 								DEFINEs								*/

// Add private define's here

/* **************************************************************** */
/* 								MACROs								*/

// Add private macro's here

/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

int tcp_connect (char * server_ip, int server_port, struct sockaddr_in *their_addr)
{
    int	sockfd;

    struct hostent *he;		// Se utiliza para convertir el nombre del host a su dirección IP

    /* Convertimos el nombre del host a su dirección IP */
    if ((he = gethostbyname ((const char *) server_ip)) == NULL)
    {
        herror("Error en gethostbyname \n");
        exit(1);
    }

    /* Creamos el socket */
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        perror("Error en creación de socket \n");

        exit(1);
    }

    /* Establecemos their_addr con la direccion del server */
    their_addr->sin_family = AF_INET;

    their_addr->sin_port = htons(server_port);

    their_addr->sin_addr = *((struct in_addr *)he->h_addr);

    bzero(&(their_addr->sin_zero), 8);

    /* Intentamos conectarnos con el servidor */
    if (connect(sockfd, (struct sockaddr *)their_addr, sizeof(struct sockaddr)) == -1)
    {
        perror("Error tratando de conectar al server \n");

        exit(1);
    }

    return sockfd;
}


int tcp_openConnection ( int server_port, struct sockaddr_in * my_addr)
{
    int	sockaux;	//socket auxiliar

    int	aux; 		//variable auxiliar

    /*Crea un socket y verifica si hubo algún error*/
    if ((sockaux = socket(AF_INET, SOCK_STREAM, 0)) == -1)
    {
        fprintf(stderr, "Error en función socket. Código de error %s\n", strerror(sockaux));
        return -1;
    }

    /* Asignamos valores a la estructura my_addr */

    my_addr->sin_family = AF_INET;				//familia de sockets INET para UNIX

    my_addr->sin_port = htons(server_port);		//convierte el entero formato PC a entero formato network

    my_addr->sin_addr.s_addr = INADDR_ANY;		//automaticamente usa la IP local

    bzero((my_addr->sin_zero), 8);         		//rellena con ceros el resto de la estructura

    //Con la estructura sockaddr_in completa, se declara en el Sistema que este proceso escuchará pedidos por la IP y el port definidos
    if ( (aux = bind (sockaux, (struct sockaddr *)my_addr, sizeof(struct sockaddr))) == -1)
    {
        fprintf(stderr, "Error en función bind. Código de error %s\n", strerror(aux));

        return -1;
    }

    //Habilitamos el socket para recibir conexiones, con una cola de conexiones en espera que tendrá como máximo el tamaño especificado en BACKLOG
    if ((aux = listen (sockaux, BACKLOG_TCP)) == -1)
    {
        fprintf(stderr, "Error en función listen. Código de error %s\n", strerror(aux));

        return -1;
    }

    return sockaux;
}

int tcp_connectionClient (int sockfd, struct sockaddr_in * their_addr)
{
    int newfd; 	// Por este socket duplicado del inicial se transaccionará*/

    unsigned int sin_size;

    sin_size = sizeof(struct sockaddr_in);

    //Se espera por conexiones
    if ((newfd = accept(sockfd, (struct sockaddr *)their_addr, &sin_size)) == -1)
    {
        fprintf(stderr, "Error en función accept. Código de error %s\n", strerror(newfd));

        return -1;
    }

    else
    {
        fprintf (stderr, "[info] Aceptar_pedidos_tcp: conexion desde:  %s\n", inet_ntoa(their_addr->sin_addr));
        return newfd;
    }
}

ssize_t tcp_sendData (int fd, void * data, int bytes)
{
    ssize_t numbytes;

    fprintf (stderr, "[info] sendDataTCP: Send data TCP \n");

    if((numbytes = send(fd, data, bytes, 0)) == -1)
    {
        perror("ERROR: Send data TCP");

        exit(-1);
    }

    fprintf (stderr, "[info] sendDataTCP: %ld numbytes Send data TCP \n", (long int)numbytes);

    return numbytes;
}

ssize_t tcp_recvData(int fd, void * data, int bytes)
{
    ssize_t numbytes;

    if((numbytes = recv(fd, data, bytes, 0))== -1)
    {
        perror("ERROR: Recive data TCP \n");

        exit(-1);
    }

    return numbytes;
}
