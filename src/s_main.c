/**
 * DOXYGEN COMMENTS
 *
 * @file   s_main.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   31/7/2015 [DD/MM/YYYY]
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include	"s_main.h"

/* **************************************************************** */
/* 								DEFINEs								*/

#define DEBUG

/* **************************************************************** */
/* 								MACROs								*/

#define		ENDLINE2NULL		for(index=0;messageBuffer[index]!='\n'&&messageBuffer[index]!='\0';index++);messageBuffer[index]='\0';index=0

#define		CLEAR_SCREEN		system("clear")


/* **************************************************************** */
/* 								GLOBALs								*/

// Add global variable's here

/* **************************************************************** */
/* 								 CODE								*/

int main(int argc, char *argv[])
{
	/** ************    VARIABLE's DECLARATION    ************ **/

	int index = 0;								// Variable utilizada para recorrer vectores y como auxiliar en lazos

	int count = 0;

	int exitVar = FALSE;						// Variable que indica si se desea salir del loop principal o no

	int selectValue = 0;

	int tcpServerFD = -1;						// Variable donde se almacena el file descriptor del socket TCP del servidor

	int udpServerFD = -1;						// Variable donde se almacena el file descriptor del socket UDP del servidor

	int tcpClientFD = -1;						// Variable donde se almacena el file descriptor del socket TCP asignado al cliente

	struct sockaddr_in tcpServerAddress;		// Estructura que contiene información útil sobre el socket TCP del servidor

	struct sockaddr_in tcpClientAddress;		// Estructura que contiene información útil sobre el socket TCP del cliente

	struct sockaddr_in udpServerAddress;		// Estructura que contiene información útil sobre el socket UDP del servidor

	struct sockaddr_in udpClientAddress;		// Estructura que contiene información útil sobre el socket UDP del cliente

	fd_set fdsMaster;							// Variable que utiliza el handler del Select

	fd_set fdsAuxiliar;							// Variable que utiliza el handler del Select

	struct timeval timeout;						// Estructura para setear el timeout del Select

	char messageBuffer[MAX_MESSAGE];			// Vector donde se guardan los mensajes recibidos

	/** ****************************************************** **/

	CLEAR_SCREEN;

	printf("\nDebug(Servidor): Abriendo sockets...\n");

	//Abro el socket UDP
	udpServerFD = udp_connectionServer( UDP_SERVER_PORT , &udpServerAddress );

	if(IS_ERROR(udpServerFD))
	{
		perror("Error al abrir el socket UDP");

		return -1;
	}

	//Abro el socket TCP para recibir clientes
	tcpServerFD = tcp_openConnection( TCP_SERVER_PORT , &tcpServerAddress );

	if(IS_ERROR(tcpServerFD))
	{
		perror("Error al abrir el socket TCP");

		return -1;
	}

	printf("\nDebug(Servidor): Los sockets se abrieron correctamente.\n");

	printf("\nDebug(Servidor): Esperando conexión de cliente...\n");

	tcpClientFD = tcp_connectionClient( tcpServerFD , &tcpClientAddress );

	printf("\nDebug(Servidor): Se ha recibido una conexión.\n");

	count = sizeof(udpClientAddress);

	recvfrom(udpServerFD,messageBuffer,MAX_MESSAGE,0,(struct sockaddr *)&udpClientAddress,(unsigned int *)&count);

	ENDLINE2NULL;

	sendto(udpServerFD,"Connection accepted",strlen("Connection accepted"),0,(struct sockaddr *)&udpClientAddress,sizeof(udpClientAddress));

	printf("\nDebug(Proceso Padre): Se recibió por UDP: \"%s\"\n",messageBuffer);

	/** ************    Seteos de la función Select    ************ **/

	FD_ZERO( &fdsMaster );

	//File Descriptos a monitorear
	FD_SET( KEYBOARD_FD, &fdsMaster );		//Seteo el teclado
	FD_SET( tcpClientFD, &fdsMaster );		//Seteo el teclado

	//Seteos timeout
	timeout.tv_sec = SEC_SELECT_TIMEOUT;
	timeout.tv_usec = USEC_SELECT_TIMEOUT;

	/** ****************************************************** **/

	/**		MAIN LOOP		**/
	while(FALSE == exitVar)
	{
		bzero(messageBuffer,MAX_MESSAGE);		//Limpio el buffer de mensajes

		fdsAuxiliar = fdsMaster;		//Copio en Auxiliar el Master, dado que el mismo se "reinicia" en cada loop

		selectValue = select( tcpClientFD+1 , &fdsAuxiliar , NULL , NULL , &timeout );

		/**
		 * SELECT STATEMENT: "Interrupción" por teclado
		 **/
		if( FD_ISSET( KEYBOARD_FD , &fdsAuxiliar ) == TRUE )
		{
			fgets(messageBuffer, MAX_MESSAGE, stdin);		//Capturo el comando ingresado por teclado

			//Reemplazo el fin de linea ('\n') por un '\0'
			ENDLINE2NULL;

			//Si las strings coinciden
			if(0 == strcmp(messageBuffer,"exit"))	exitFunction(&exitVar,tcpClientFD,"exit");
		}

		/**
		 * SELECT STATEMENT: "Interrupción" por socket
		 **/
		else if( FD_ISSET( tcpClientFD , &fdsAuxiliar ) == TRUE )
		{
			read(tcpClientFD, messageBuffer, MAX_MESSAGE);

			if(0 == strcmp("exit",messageBuffer))
			{
				printf("\nDebug(Cliente): Se recibió el comando de escape por el socket...\n");

				exitVar = TRUE;
			}

		}

		/**
		 * SELECT STATEMENT: "Interrupción" por timeout
		 **/
		else if( IS_TIMEOUT( selectValue ) )
		{
			count++;

			sprintf(messageBuffer,"%d",count);

			sendto(udpServerFD,messageBuffer,strlen(messageBuffer),0,(struct sockaddr *)&udpClientAddress,sizeof(udpClientAddress));
		}

	}

	close(tcpServerFD);

	close(udpServerFD);

	printf("\nDebug(Servidor): Se cierra el Servidor.\n");

	return 0;
}

