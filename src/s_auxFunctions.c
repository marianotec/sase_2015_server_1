/**
 * @file   server_auxiliar_functions.c
 *
 * @Author Koremblum, Nicolás Mariano (marianotec7@gmail.com)
 * 		   Di Donato, Andrés (adidonato@gmail.com)
 *
 * @date   15/7/2015
 *
 * @brief  Brief description of file.
 *
 * Detailed description of file.
 *
 * @todo   What's to do?
 */

/* **************************************************************** */
/* 								INCLUDEs							*/

#include "s_auxFunctions.h"

/* **************************************************************** */
/* 								GLOBALs								*/

/* **************************************************************** */
/* 								DEFINEs								*/

#define		TRUE		1

/* **************************************************************** */
/* 								MACROs								*/

#define		IS_ERROR(x)		( (0 > x)  ? 1:0 )

/* **************************************************************** */
/* 								 CODE								*/

void exitFunction(int *exit, int wFD, const char *message)
{
	int returnValue = 0;

	if(exit)	*exit = TRUE;

	//Informo al proceso hijo que se desea cerrar el programa.
	returnValue = writeTo(wFD, message);

	if(IS_ERROR(returnValue))	*exit = returnValue;

	return;
}



int writeTo(int wFD, const char * message)
{
	int returnValue = 0;

	returnValue = write(wFD, message, strlen(message));

	if(IS_ERROR(returnValue))
	{
		returnValue = write(wFD, message, strlen(message));

		if(IS_ERROR(returnValue))	return WRITE_ERROR_CODE;
	}

	return 0;
}
